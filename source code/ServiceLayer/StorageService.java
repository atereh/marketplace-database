package source.ServiceLayer;

import source.DAOLayer.ProductDAO.ProductDAO;
import source.DAOLayer.ProductStoreDAO.ProductStoreDAO;
import source.DAOLayer.StorageManager;
import source.DAOLayer.StoreDAO.StoreDAO;
import source.ModelObjects.*;
import source.ModelObjects.Product;
import source.ModelObjects.ProductStore;
import source.ModelObjects.Store;

import java.math.BigDecimal;
import java.util.List;

public class StorageService {
    private ProductDAO productDAO;
    private StoreDAO storeDAO;
    private ProductStoreDAO productStoreDAO;

    public StorageService() {
        StorageManager storageManager = new StorageManager();
        this.productDAO = storageManager.getProductDAO();
        this.storeDAO = storageManager.getStoreDAO();
        this.productStoreDAO = storageManager.getProductStoreDAO();
    }

    public void addNewStore(Store store) {
        List<Store> allStores = storeDAO.getAllStores();
        if (!allStores.contains(store)) {
            storeDAO.addStore(store);
        }
    }

    public void addNewProduct(Product product) {
        List<Product> allProducts = productDAO.getAllProducts();
        if (!allProducts.contains(product)) {
            productDAO.addProduct(product);
        }
    }

    public void conveyProducts(ProductStore productStore) {
        List<ProductStore> allProducStores = productStoreDAO.getAllProductStores();
        if (allProducStores.contains(productStore)) {
            productStoreDAO.updateProductStore(productStore);
        } else {
            productStoreDAO.addProductStore(productStore);
        }
    }

    public BigDecimal buyProductsFromStore(Product product, Store store, int orderQuantity) {
        ProductStore wantedProductStore = new ProductStore(product.getProductID(), store.getStoreID());
        List<ProductStore> allProductStores = productStoreDAO.getAllProductStores();
        for (ProductStore currentProductStore: allProductStores) {
            if (currentProductStore.equals(wantedProductStore)) {
                int updatedQuantity = currentProductStore.getProductQuantity() - orderQuantity;
                if (updatedQuantity < 0) {
                    return null;
                } else {
                    currentProductStore.setProductQuantity(updatedQuantity);
                    productStoreDAO.updateProductStore(currentProductStore);
                    return currentProductStore.getProductPrice().multiply(new BigDecimal(orderQuantity));
                }
            }
        }
        return null;
    }

    public Store findCheapestStoreForProduct(Product product) {
        Store cheapestStore = null;
        int cheapestStoreID = -1;
        List<ProductStore> allProductStores = productStoreDAO.getAllProductStores();
        BigDecimal cheapestPrice = new BigDecimal(Integer.MAX_VALUE);
        for (ProductStore productStore: allProductStores) {
            if (productStore.getProductID() == product.getProductID()) {
                if (productStore.getProductPrice().compareTo(cheapestPrice) < 0) {
                    cheapestStoreID = productStore.getStoreID();
                }
            }
        }
        if (cheapestStoreID == -1) {
            return null;
        }

        for (Store currentStore: storeDAO.getAllStores()) {
            if (currentStore.getStoreID() == cheapestStoreID) {
                cheapestStore = currentStore;
                break;
            }
        }
        return cheapestStore;
    }
}
