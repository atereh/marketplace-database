package source;

import source.DAOLayer.ProductDAO.ProductDAO;
import source.DAOLayer.ProductStoreDAO.ProductStoreDAO;
import source.DAOLayer.StorageManager;
import source.DAOLayer.StoreDAO.StoreDAO;
import source.ModelObjects.Product;
import source.ModelObjects.ProductStore;
import source.ModelObjects.Store;

import java.math.BigDecimal;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        StorageManager testManager = new StorageManager();
        ProductDAO productDAO = testManager.getProductDAO();
        StoreDAO storeDAO = testManager.getStoreDAO();
        ProductStoreDAO productStoreDAO = testManager.getProductStoreDAO();

        Product newProd = new Product("test product");
        Store newStore = new Store("Achaun");

        productDAO.addProduct(newProd);
        storeDAO.addStore(newStore);

        ProductStore newProductStore = new ProductStore(newProd.getProductID(), newStore.getStoreID(), 100,new BigDecimal(10.0));
        productStoreDAO.addProductStore(newProductStore);

        newProd.setProductName("super test product");
        newStore.setStoreName("5'rochka");
        newStore.setStoreAddress("SPb, Nevsky pr., 22");
        newProductStore.setProductQuantity(50);

        productDAO.updateProduct(newProd);
        storeDAO.updateStore(newStore);
        productStoreDAO.updateProductStore(newProductStore);

        List<ProductStore> allProductStores = productStoreDAO.getAllProductStores();
        for (ProductStore productStore: allProductStores) {
            System.out.println(
                    productStore.getProductID() + "\t" +
                    productStore.getStoreID() + "\t" +
                    productStore.getProductQuantity() + "\t" +
                    productStore.getProductPrice()
            );
        }
        productStoreDAO.deleteProductStore(newProductStore);

        List<Product> allProducts = productDAO.getAllProducts();
        for (Product product: allProducts) {
            System.out.println(product.getProductID() + "\t" + product.getProductName());
        }
        productDAO.deleteProduct(newProd);

        List<Store> allStores = storeDAO.getAllStores();
        for (Store store: allStores) {
            System.out.println(store.getStoreID() + "\t" + store.getStoreName() + "\t" + store.getStoreAddress() + "\t" + store.getStoreCity());
        }
        //storeDAO.deleteStore(newStore);
    }
}
