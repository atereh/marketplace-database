package source.ModelObjects;

import java.math.BigDecimal;
import java.util.Objects;

public class ProductStore {
    private int productID;
    private int storeID;
    private int productQuantity;
    private BigDecimal productPrice;

    private static final int defaultQuantity = 0;
    private static final BigDecimal defaultPrice = new BigDecimal(0);

    public ProductStore(int productID, int storeID) {
        this.productID = productID;
        this.storeID = storeID;
        this.productQuantity = defaultQuantity;
        this.productPrice = defaultPrice;
    }

    public ProductStore(int productID, int storeID, int productQuantity, BigDecimal productPrice) {
        this.productID = productID;
        this.storeID = storeID;
        this.productQuantity = productQuantity;
        this.productPrice = productPrice;
    }

    public String convertToCSV() {
        return storeID + "," + productID + "," + productQuantity + "," + productPrice;
    }

    public int getProductID() {
        return productID;
    }

    public void setProductID(int productID) {
        this.productID = productID;
    }

    public int getStoreID() {
        return storeID;
    }

    public void setStoreID(int storeID) {
        this.storeID = storeID;
    }

    public int getProductQuantity() {
        return productQuantity;
    }

    public void setProductQuantity(int productQuantity) {
        this.productQuantity = productQuantity;
    }

    public BigDecimal getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(BigDecimal productPrice) {
        this.productPrice = productPrice;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductStore that = (ProductStore) o;
        return productID == that.productID &&
                storeID == that.storeID;
    }

    @Override
    public int hashCode() {
        return Objects.hash(productID, storeID);
    }
}
