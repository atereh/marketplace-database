package source.ModelObjects;

import java.util.Objects;

public class Store {
    private int storeID;
    private String storeName;
    private String storeAddress;

    private static final int defaultID = -1;
    private static final String defaultAddress = " ";

    public Store(String storeName) {
        this.storeID = defaultID;
        this.storeName = storeName;
        this.storeAddress = defaultAddress;
    }

    public Store(int storeID, String storeName) {
        this.storeID = storeID;
        this.storeName = storeName;
        this.storeAddress = defaultAddress;
    }

    public Store(int storeID, String storeName, String storeAddress) {
        this.storeID = storeID;
        this.storeName = storeName;
        this.storeAddress = storeAddress;
    }

    public String convertToCSV() {
        return storeID + "," + storeName.replace(',','¡') + "," + storeAddress.replace(',','¡');
    }

    public int getStoreID() {
        return storeID;
    }

    public void setStoreID(int storeID) {
        this.storeID = storeID;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getStoreAddress() {
        return storeAddress;
    }

    public void setStoreAddress(String storeAddress) {
        this.storeAddress = storeAddress;
    }

    public String getStoreCity() {
        String[] parts = storeAddress.split(",");
        if(parts.length > 0) return parts[0];
        return "";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Store store = (Store) o;
        return storeID == store.storeID;
    }

    @Override
    public int hashCode() {
        return Objects.hash(storeID);
    }
}
