package source.ModelObjects;

import java.util.Objects;

public class Product {
    private int productID;
    private String productName;

    private static final int defaultID = -1;

    public Product(String productName) {
        this.productID = defaultID;
        this.productName = productName;
    }

    public Product(Integer productID, String productName) {
        this.productID = productID;
        this.productName = productName;
    }

    public int getProductID() {
        return productID;
    }

    public void setProductID(int productID) {
        this.productID = productID;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String convertToCSV() {
        return productID + "," + productName.replace(',','¡');
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return productID == product.productID;
    }

    @Override
    public int hashCode() {
        return Objects.hash(productID);
    }
}
