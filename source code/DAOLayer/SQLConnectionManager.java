package source.DAOLayer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class SQLConnectionManager {

    private String host;
    private String user;
    private String pass;

    SQLConnectionManager(String host, String user, String pass) {
        this.host = host;
        this.user = user;
        this.pass = pass;
    }

    public Connection openSQLConnection() {
        Connection con = null;
        try {
            con = DriverManager.getConnection(host, user, pass);
        } catch (SQLException e) {
            System.out.println("COULDN'T OPEN A NEW CONNECTION:\t" + e.getLocalizedMessage());
        }
        return con;
    }

    public void closeSQLConnection(Connection con) {
        try {
            con.close();
        } catch (SQLException e) {
            System.out.println("COULDN'T CLOSE A CONNECTION:\t" + e.getLocalizedMessage());
        }
    }
}
