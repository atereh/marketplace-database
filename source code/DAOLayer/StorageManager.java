package source.DAOLayer;

import source.DAOLayer.ProductDAO.Implementations.ProductDAOCSVImpl;
import source.DAOLayer.ProductDAO.Implementations.ProductDAOSQLImpl;
import source.DAOLayer.ProductDAO.ProductDAO;
import source.DAOLayer.ProductStoreDAO.Implementations.ProductStoreDAOCSVImpl;
import source.DAOLayer.ProductStoreDAO.Implementations.ProductStoreDAOSQLImpl;
import source.DAOLayer.ProductStoreDAO.ProductStoreDAO;
import source.DAOLayer.StoreDAO.Implementations.StoreDAOCSVImpl;
import source.DAOLayer.StoreDAO.Implementations.StoreDAOSQLImpl;
import source.DAOLayer.StoreDAO.StoreDAO;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class StorageManager {

    private ProductDAO productDAO;
    private StoreDAO storeDAO;
    private ProductStoreDAO productStoreDAO;

    enum StorageTypes {
        sql,
        csv
    }

    public StorageManager() {

        FileInputStream fis;
        Properties property = new Properties();

        try {
            fis = new FileInputStream("src/main/resources/config.properties");
            property.load(fis);

            String type = property.getProperty("storage.type");
            if (type.equals(StorageTypes.sql.name())) {
                String host = property.getProperty("db.host");
                String login = property.getProperty("db.login");
                String pass = property.getProperty("db.password");

                SQLConnectionManager sqlConnectionManager = new SQLConnectionManager(host, login, pass);

                storeDAO = new StoreDAOSQLImpl(sqlConnectionManager);
                productDAO = new ProductDAOSQLImpl(sqlConnectionManager);
                productStoreDAO = new ProductStoreDAOSQLImpl(sqlConnectionManager);
            } else if (type.equals(StorageTypes.csv.name())) {
                String productStorageLocation = "src/main/resources/Product.csv";
                String storeStorageLocation = "src/main/resources/Store.csv";
                String productStoreStorageLocation = "src/main/resources/ProductStore.csv";
                CSVConnectionManager csvConnectionManager = new CSVConnectionManager(productStorageLocation,
                        storeStorageLocation, productStoreStorageLocation);

                storeDAO = new StoreDAOCSVImpl(csvConnectionManager);
                productDAO = new ProductDAOCSVImpl(csvConnectionManager);
                productStoreDAO = new ProductStoreDAOCSVImpl(csvConnectionManager);
            }
        } catch (IOException e) {
            System.out.println("STORAGE CONFIGURATION ERROR:\t" + e.getLocalizedMessage());
        }
    }

    public ProductDAO getProductDAO() {
        return productDAO;
    }

    public StoreDAO getStoreDAO() {
        return storeDAO;
    }

    public ProductStoreDAO getProductStoreDAO() {
        return productStoreDAO;
    }
}
