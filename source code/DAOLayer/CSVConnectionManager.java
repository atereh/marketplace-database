package source.DAOLayer;

import java.io.File;

public class CSVConnectionManager {
    private String productStorageLocation;
    private String storeStorageLocation;
    private String productStoreStorageLocation;

    public CSVConnectionManager(String productStorageLocation,
                                String storeStorageLocation,
                                String productStoreStorageLocation) {
        this.productStorageLocation = productStorageLocation;
        this.storeStorageLocation = storeStorageLocation;
        this.productStoreStorageLocation = productStoreStorageLocation;
    }

    public File getProductStorage() {
        return new File(productStorageLocation);
    }

    public File getStoreStorage() {
        return new File(storeStorageLocation);
    }

    public File getProductStoreStorage() {
        return new File(productStoreStorageLocation);
    }
}
