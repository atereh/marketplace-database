package source.DAOLayer.StoreDAO.Implementations;

import source.DAOLayer.SQLConnectionManager;
import source.DAOLayer.StoreDAO.StoreDAO;
import source.ModelObjects.Store;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("ALL")
public class StoreDAOSQLImpl implements StoreDAO {

    private SQLConnectionManager sqlConnectionManager;

    public StoreDAOSQLImpl(SQLConnectionManager sqlConnectionManager) {
        this.sqlConnectionManager = sqlConnectionManager;
    }

    @Override
    public List<Store> getAllStores() {
        List<Store> storesList = new ArrayList<>();

        String sql = "SELECT * FROM Store";

        Connection con = sqlConnectionManager.openSQLConnection();

        try {
            Statement statement = con.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                Integer id = resultSet.getInt("StoreID");
                String name = resultSet.getString("StoreName");
                Store store = new Store(id, name);
                storesList.add(store);
            }
        } catch (SQLException e) {
            System.out.println("FAILED TO GET ALL STORES:\t" + e.getLocalizedMessage());
        } finally {
            sqlConnectionManager.closeSQLConnection(con);
        }
        return storesList;
    }

    @Override
    public void addStore(Store store) {
        String sql = "INSERT INTO Store VALUES (?)";

        Connection con = sqlConnectionManager.openSQLConnection();

        try {
            PreparedStatement preparedStatement = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, store.getStoreName());

            preparedStatement.executeUpdate();

            // get last ProductID of the last inserted item
            ResultSet generatedID = preparedStatement.getGeneratedKeys();
            if (generatedID.next()) {
                Integer newID = generatedID.getInt(1);
                store.setStoreID(newID);
            } else {
                throw new SQLException("Creating Store failed, no ID obtained.");
            }
        } catch (SQLException e) {
            System.out.println("FAILED TO ADD A NEW STORE:\t" + e.getLocalizedMessage());
        } finally {
            sqlConnectionManager.closeSQLConnection(con);
        }
    }

    @Override
    public void updateStore(Store store) {
        String sql = "UPDATE Store SET StoreName = ? WHERE StoreID = ?";

        Connection con = sqlConnectionManager.openSQLConnection();

        try {
            PreparedStatement preparedStatement = con.prepareStatement(sql);
            preparedStatement.setString(1, store.getStoreName());
            preparedStatement.setInt(2, store.getStoreID());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.println("FAILED TO UPDATE A STORE:\t" + e.getLocalizedMessage());
        } finally {
            sqlConnectionManager.closeSQLConnection(con);
        }
    }

    @Override
    public void deleteStore(Store store) {
        String sql = "DELETE FROM Store WHERE StoreID = ?";

        Connection con = sqlConnectionManager.openSQLConnection();

        try {
            PreparedStatement preparedStatement = con.prepareStatement(sql);
            preparedStatement.setInt(1, store.getStoreID());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.println("FAILED TO DELETE A STORE:\t" + e.getLocalizedMessage());
        } finally {
            sqlConnectionManager.closeSQLConnection(con);
        }
    }
}
