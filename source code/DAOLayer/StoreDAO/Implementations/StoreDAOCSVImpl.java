package source.DAOLayer.StoreDAO.Implementations;

import source.DAOLayer.CSVConnectionManager;
import source.DAOLayer.StoreDAO.StoreDAO;
import source.ModelObjects.Store;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class StoreDAOCSVImpl implements StoreDAO {
    private CSVConnectionManager connectionManager;

    public StoreDAOCSVImpl(CSVConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    @Override
    public List<Store> getAllStores() {
        File file = connectionManager.getStoreStorage();
        List<Store> storeList = new ArrayList<>();

        try (Scanner scanner = new Scanner(file)) {
            while (scanner.hasNextLine()) {
                String[] row = scanner.nextLine().split(",");
                Integer id = Integer.valueOf(row[0]);
                String name = row[1];
                String address = row[2];
                Store store = new Store(id, name.replace('¡',','), address.replace('¡',','));

                storeList.add(store);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return storeList;
    }

    @Override
    public void addStore(Store store) {
        File file = connectionManager.getStoreStorage();

        try (FileWriter fileWriter = new FileWriter(file)) {
            fileWriter.write(store.convertToCSV() + '\n');
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateStore(Store store) {
        File file = connectionManager.getStoreStorage();
        File temp = new File("src/main/resources/temp.csv");

        try (FileWriter fileWriter = new FileWriter(temp, true);
             Scanner scanner = new Scanner(file)) {
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                if (Integer.valueOf(line.split(",")[0]).equals(store.getStoreID())) {
                    line = store.convertToCSV();
                }
                fileWriter.write(line + '\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        file.delete();
        temp.renameTo(file);
    }

    @Override
    public void deleteStore(Store store) {
        File file = connectionManager.getStoreStorage();

        File temp = new File("src/main/resources/temp.csv");
        try (FileWriter fileWriter = new FileWriter(temp, true);
             Scanner scanner = new Scanner(file)) {
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                if (!line.contains(store.convertToCSV())) {
                    fileWriter.write(line + '\n');
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        file.delete();
        temp.renameTo(file);
    }
}
