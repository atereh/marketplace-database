package source.DAOLayer.StoreDAO;

import source.ModelObjects.Store;

import java.util.List;

public interface StoreDAO {
    List<Store> getAllStores();
    void addStore(Store store);
    void updateStore(Store store);
    void deleteStore(Store store);
}
