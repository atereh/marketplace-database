package source.DAOLayer.ProductDAO;

import source.ModelObjects.Product;

import java.util.List;

public interface ProductDAO {
    List<Product> getAllProducts();
    void addProduct(Product product);
    void updateProduct(Product product);
    void deleteProduct(Product product);
}
