package source.DAOLayer.ProductDAO.Implementations;

import source.DAOLayer.ProductDAO.ProductDAO;
import source.DAOLayer.SQLConnectionManager;
import source.ModelObjects.Product;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("ALL")
public class ProductDAOSQLImpl implements ProductDAO {

    private SQLConnectionManager sqlConnectionManager;

    public ProductDAOSQLImpl(SQLConnectionManager sqlConnectionManager) {
        this.sqlConnectionManager = sqlConnectionManager;
    }

    @Override
    public List<Product> getAllProducts() {
        List<Product> productsList = new ArrayList<>();

        String sql = "SELECT * FROM Product";

        Connection con = sqlConnectionManager.openSQLConnection();

        try {
            Statement statement = con.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                Integer id = resultSet.getInt("ProductID");
                String name = resultSet.getString("ProductName");
                Product store = new Product(id, name);
                productsList.add(store);
            }
        } catch (SQLException e) {
            System.out.println("FAILED TO GET ALL PRODUCTS:\t" + e.getLocalizedMessage());
        } finally {
            sqlConnectionManager.closeSQLConnection(con);
        }
        return productsList;
    }

    @Override
    public void addProduct(Product product) {
        String sql = "INSERT INTO Product VALUES (?)";

        Connection con = sqlConnectionManager.openSQLConnection();

        try {
            PreparedStatement preparedStatement = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, product.getProductName());

            preparedStatement.executeUpdate();

            // get last ProductID of the last inserted item
            ResultSet generatedID = preparedStatement.getGeneratedKeys();
            if (generatedID.next()) {
                Integer newID = generatedID.getInt(1);
                product.setProductID(newID);
            } else {
                throw new SQLException("Creating Product failed, no ID obtained.");
            }
        } catch (SQLException e) {
            System.out.println("FAILED TO ADD A NEW PRODUCT:\t" + e.getLocalizedMessage());
        } finally {
            sqlConnectionManager.closeSQLConnection(con);
        }
    }

    @Override
    public void updateProduct(Product product) {
        String sql = "UPDATE Product SET ProductName = ? WHERE ProductID = ?";

        Connection con = sqlConnectionManager.openSQLConnection();

        try {
            PreparedStatement preparedStatement = con.prepareStatement(sql);
            preparedStatement.setString(1, product.getProductName());
            preparedStatement.setInt(2, product.getProductID());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.println("FAILED TO UPDATE A PRODUCT:\t" + e.getLocalizedMessage());
        } finally {
            sqlConnectionManager.closeSQLConnection(con);
        }
    }

    @Override
    public void deleteProduct(Product product) {
        String sql = "DELETE FROM Product WHERE ProductID = ?";

        Connection con = sqlConnectionManager.openSQLConnection();

        try {
            PreparedStatement preparedStatement = con.prepareStatement(sql);
            preparedStatement.setInt(1, product.getProductID());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.println("FAILED TO DELETE A PRODUCT:\t" + e.getLocalizedMessage());
        } finally {
            sqlConnectionManager.closeSQLConnection(con);
        }
    }
}
