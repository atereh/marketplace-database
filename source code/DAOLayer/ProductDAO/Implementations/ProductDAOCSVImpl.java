package source.DAOLayer.ProductDAO.Implementations;

import source.DAOLayer.CSVConnectionManager;
import source.DAOLayer.ProductDAO.ProductDAO;
import source.ModelObjects.Product;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ProductDAOCSVImpl implements ProductDAO {
    private CSVConnectionManager connectionManager;

    public ProductDAOCSVImpl(CSVConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    @Override
    public List<Product> getAllProducts() {
        File file = connectionManager.getProductStorage();
        List<Product> productList = new ArrayList<>();

        try (Scanner scanner = new Scanner(file)) {
            while (scanner.hasNextLine()) {
                String[] row = scanner.nextLine().split(",");
                Integer id = Integer.valueOf(row[0]);
                String name = row[1];
                Product product = new Product(id, name.replace('¡',','));

                productList.add(product);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return productList;
    }

    @Override
    public void addProduct(Product product) {
        File file = connectionManager.getProductStorage();
        try (FileWriter fileWriter = new FileWriter(file)) {
            fileWriter.write(product.convertToCSV() + '\n');
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateProduct(Product product) {
        File file = connectionManager.getProductStorage();
        File temp = new File("src/main/resources/temp.csv");

        try (FileWriter fileWriter = new FileWriter(temp, true);
             Scanner scanner = new Scanner(file)) {
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                if (Integer.valueOf(line.split(",")[0]).equals(product.getProductID())) {
                    line = product.convertToCSV();
                }
                fileWriter.write(line + '\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        file.delete();
        temp.renameTo(file);
    }

    @Override
    public void deleteProduct(Product product) {
        File file = connectionManager.getProductStorage();

        File temp = new File("src/main/resources/temp.csv");
        try (FileWriter fileWriter = new FileWriter(temp, true);
             Scanner scanner = new Scanner(file)) {
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                if (!line.contains(product.convertToCSV())) {
                    fileWriter.write(line + '\n');
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        file.delete();
        temp.renameTo(file);
    }
}
