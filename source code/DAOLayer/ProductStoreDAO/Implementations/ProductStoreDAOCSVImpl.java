package source.DAOLayer.ProductStoreDAO.Implementations;

import source.DAOLayer.CSVConnectionManager;
import source.DAOLayer.ProductStoreDAO.ProductStoreDAO;
import source.ModelObjects.ProductStore;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ProductStoreDAOCSVImpl implements ProductStoreDAO {
    private CSVConnectionManager connectionManager;

    public ProductStoreDAOCSVImpl(CSVConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    @Override
    public List<ProductStore> getAllProductStores() {
        List<ProductStore> storeProductList = new ArrayList<>();
        File file = connectionManager.getProductStoreStorage();

        try {
            Scanner scanner = new Scanner(file);
            while (scanner.hasNextLine()) {
                String[] row = scanner.nextLine().split(",");
                Integer productID = Integer.valueOf(row[1]);
                Integer storeID = Integer.valueOf(row[0]);
                Integer quantity = Integer.valueOf(row[2]);
                BigDecimal cost = BigDecimal.valueOf(Integer.valueOf(row[3]));
                ProductStore storeProduct = new ProductStore(productID, storeID, quantity, cost);

                storeProductList.add(storeProduct);
            }
        } catch (IOException e) {
            System.out.println("FAILED TO GET ALL ProductStores:\t" + e.getLocalizedMessage());
        }
        return storeProductList;
    }

    @Override
    public void addProductStore(ProductStore productStore) {
        File file = connectionManager.getProductStoreStorage();

        try (FileWriter fileWriter = new FileWriter(file)) {
            fileWriter.write(productStore.convertToCSV() + '\n');
        } catch (IOException e) {
            System.out.println("FAILED TO ADD A NEW ProductStore:\t" + e.getLocalizedMessage());
        }
    }

    @Override
    public void updateProductStore(ProductStore productStore) {
        File temp = new File("src/main/resources/temp.csv");
        File file = connectionManager.getProductStoreStorage();

        try (FileWriter fileWriter = new FileWriter(temp);
             Scanner scanner = new Scanner(file)) {
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                if (Integer.valueOf(line.split(",")[0]).equals(productStore.getStoreID()) &&
                        Integer.valueOf(line.split(",")[1]).equals(productStore.getProductID())) {
                    line = productStore.convertToCSV();
                }
                fileWriter.write(line + '\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        file.delete();
        temp.renameTo(file);
    }

    @Override
    public void updateProductStoreQuantity(ProductStore productStore) {

    }

    @Override
    public void deleteProductStore(ProductStore productStore) {

    }
}
