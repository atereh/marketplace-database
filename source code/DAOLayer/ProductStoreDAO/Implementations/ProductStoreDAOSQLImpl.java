package source.DAOLayer.ProductStoreDAO.Implementations;

import source.DAOLayer.ProductStoreDAO.ProductStoreDAO;
import source.DAOLayer.SQLConnectionManager;
import source.ModelObjects.ProductStore;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("ALL")
public class ProductStoreDAOSQLImpl implements ProductStoreDAO {

    private SQLConnectionManager sqlConnectionManager;

    public ProductStoreDAOSQLImpl(SQLConnectionManager sqlConnectionManager) {
        this.sqlConnectionManager = sqlConnectionManager;
    }

    @Override
    public List<ProductStore> getAllProductStores() {
        List<ProductStore> productStoresList = new ArrayList<>();

        String sql = "SELECT * FROM ProductStore";

        Connection con = sqlConnectionManager.openSQLConnection();

        try {
            Statement statement = con.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                Integer productID = resultSet.getInt("ProductID");
                Integer storeID = resultSet.getInt("StoreID");
                Integer quantity = resultSet.getInt("ProductQuantity");
                BigDecimal price = resultSet.getBigDecimal("ProductPrice");
                ProductStore store = new ProductStore(productID, storeID, quantity, price);
                productStoresList.add(store);
            }
        } catch (SQLException e) {
            System.out.println("FAILED TO GET ALL ProductStores:\t" + e.getLocalizedMessage());
        } finally {
            sqlConnectionManager.closeSQLConnection(con);
        }
        return productStoresList;
    }

    @Override
    public void addProductStore(ProductStore productStore) {
        String sql = "INSERT INTO ProductStore VALUES (?, ?, ?, ?)";

        Connection con = sqlConnectionManager.openSQLConnection();

        try {
            PreparedStatement preparedStatement = con.prepareStatement(sql);
            preparedStatement.setInt(1, productStore.getProductID());
            preparedStatement.setInt(2, productStore.getStoreID());
            preparedStatement.setInt(3, productStore.getProductQuantity());
            preparedStatement.setBigDecimal(4, productStore.getProductPrice());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.println("FAILED TO ADD A NEW ProductStore:\t" + e.getLocalizedMessage());
        } finally {
            sqlConnectionManager.closeSQLConnection(con);
        }
    }

    @Override
    public void updateProductStore(ProductStore productStore) {
        String sql = "UPDATE ProductStore SET ProductQuantity = ?, ProductPrice = ? " +
                "WHERE ProductID = ? AND StoreID = ?";

        Connection con = sqlConnectionManager.openSQLConnection();

        try {
            PreparedStatement preparedStatement = con.prepareStatement(sql);
            preparedStatement.setInt(1, productStore.getProductQuantity());
            preparedStatement.setBigDecimal(2, productStore.getProductPrice());
            preparedStatement.setInt(3, productStore.getProductID());
            preparedStatement.setInt(4, productStore.getStoreID());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.println("FAILED TO UPDATE A ProductStore:\t" + e.getLocalizedMessage());
        } finally {
            sqlConnectionManager.closeSQLConnection(con);
        }
    }

    @Override
    public void updateProductStoreQuantity(ProductStore productStore) {
        String sql = "UPDATE ProductStore SET ProductQuantity = ? " +
                "WHERE ProductID = ? AND StoreID = ?";

        Connection con = sqlConnectionManager.openSQLConnection();

        try {
            PreparedStatement preparedStatement = con.prepareStatement(sql);
            preparedStatement.setInt(1, productStore.getProductQuantity());
            preparedStatement.setInt(2, productStore.getProductID());
            preparedStatement.setInt(3, productStore.getStoreID());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.println("FAILED TO UPDATE A ProductStore:\t" + e.getLocalizedMessage());
        } finally {
            sqlConnectionManager.closeSQLConnection(con);
        }
    }

    @Override
    public void deleteProductStore(ProductStore productStore) {
        String sql = "DELETE FROM ProductStore " +
                "WHERE ProductID = ? AND StoreID = ? AND ProductQuantity = ? AND ProductPrice = ?";

        Connection con = sqlConnectionManager.openSQLConnection();

        try {
            PreparedStatement preparedStatement = con.prepareStatement(sql);
            preparedStatement.setInt(1, productStore.getProductID());
            preparedStatement.setInt(2, productStore.getStoreID());
            preparedStatement.setInt(3, productStore.getProductQuantity());
            preparedStatement.setBigDecimal(4, productStore.getProductPrice());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.println("FAILED TO DELETE A ProductStore:\t" + e.getLocalizedMessage());
        } finally {
            sqlConnectionManager.closeSQLConnection(con);
        }
    }
}
