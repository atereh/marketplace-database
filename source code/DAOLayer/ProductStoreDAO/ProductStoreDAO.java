package source.DAOLayer.ProductStoreDAO;

import source.ModelObjects.ProductStore;

import java.util.List;

public interface ProductStoreDAO {
    List<ProductStore> getAllProductStores();
    void addProductStore(ProductStore productStore);
    void updateProductStore(ProductStore productStore);
    void updateProductStoreQuantity(ProductStore productStore);
    void deleteProductStore(ProductStore productStore);
}
