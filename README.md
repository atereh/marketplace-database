### Marketplace database project

There are **Products** that are sold in **Stores**. The stores have a unique code, name (you can use a unique one), and address. 
**Products** must have a unique name. 
Each store has its own price for the product and there is a certain number of units of the product available (also some goods may not be)


### Architecture specifications

Write methods for the following operations:

1) Create a Store

2) Create a Product

3) Deliver a banch of products to the store (set of products-quantity with the ability to set / change the price)

4) Find the Store where a certain product is the cheapest

5) Understand what products you can buy in a Store for a certain amount (for example, you can buy three ice creams or two chocolates for 100 rubles)

6) Buy a bunch of products in the store (parameters - how many products to buy, the method returns the total cost of the purchase or its impossibility if the product is not enough)

7) Find which store has the smallest amount (in total) of products (set of products-quantity). For example, "which store is the cheapest to buy 10 nails and 20 screws". Product availability in stores is taken into account!

(everywhere here "Store" is a specific store whose code or name is passed in the parameters)


### Task specification

The application must contain separate layers – the client (in the simplest case client is the main function) accesses the **service layer**.
The service layer goes to the **DAO layer** for data. The DAO layer has two implementations – a relational database or *.csv* files. Which implementation is enabled is defined in the configuration file (.property). 
The program creates the necessary implementations and service classes at the start, which can then be used by the client test method.

**Requirements for a relational database:** 
you can define schema and the queries also, but **do not use stored procedures**.

**File requirements:** 

two files – for stores and for products in .csv format. 


**For stores:** 

each line has two values – the store code and name 

**Example:**

1, Magnet

2, Auchan


**For items:** 

each line - next item name, followed by three types – code of the store where goods are sold, amount in store and price

**Example:**

Chocolate ' Alenka’,1,50,40.00,2,60,45.00

PHILIPS TV, 2, 1, 21000.00


*As an added bonus (and for convenience), you can write a script (with a separate entry point) to convert data from files to a database and back*

### Contributors:
Alina Terekhova